import Vue from 'vue'
import App from './App.vue'
import router from './router'
import './plugins/element.js'
import './assets/css/global.css'
import axios from "axios";
import TreeTable from 'vue-table-with-tree-grid'
import {fmtDate} from './assets/js/Data'
//导入富文本编辑器
import VueQuillEditor from 'vue-quill-editor'

import 'quill/dist/quill.core.css' // import styles
import 'quill/dist/quill.snow.css' // for snow theme
import 'quill/dist/quill.bubble.css' // for bubble theme


Vue.config.productionTip = false

//配置请求根路径
Vue.prototype.$http = axios
axios.defaults.baseURL = 'http://www.ysqorz.top:8888/api/private/v1/'

axios.interceptors.request.use(config=>{
  // console.log(config)
  config.headers.Authorization = window.sessionStorage.getItem("token");
  return config;
})

Vue.use(VueQuillEditor)

new Vue({
  router,
  render: h => h(App)
}).$mount('#app')

Vue.component('tree-table',TreeTable)


Vue.filter('dateFormat',function (time) {
  let t = new Date(time);
  return fmtDate(t, 'yyyy-MM-dd hh:mm:ss');
})



